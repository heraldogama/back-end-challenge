const express = require("express");

const { model } = require("./services/conversations");

const app = express();

app.route("/insert").all((req, res, next) => {
  model.create(req.body);
});

const port = 3001;

app.listen(port, () => {
  console.log("\nServer is running on port 3001.");
});
